import { Component, OnInit } from '@angular/core';
import { HttpClientService } from '../service/http-client.service';

@Component({
  selector: 'app-user-home',
  templateUrl: './user-home.component.html',
  styleUrls: ['./user-home.component.css']
})
export class UserHomeComponent implements OnInit {

  constructor(private httpClientService:HttpClientService) { }

  ngOnInit(): void {
         this.loadHome();
         this.httpClientService.httpClient.get<any>('http://localhost:8070/userr');
         console.log("a");
  }

  loadHome(){
      //this.httpClientService.getUser().subscribe(
     //response =>this.handleSuccessfulResponse(response),
    //);
    return this.httpClientService.httpClient.get<any>('http://localhost:8070/user');
}

}
