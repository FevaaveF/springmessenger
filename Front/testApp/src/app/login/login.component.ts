import { Component, OnInit } from '@angular/core';
import { HttpClientService, User } from '../service/http-client.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  result:User;

  constructor(
  private httpClientService:HttpClientService,
  private router:Router
  ) {
    this.result = new User("","","");
  }

  ngOnInit(): void {
      this.httpClientService.getToken().subscribe(
     response =>this.handleSuccessfulResponse(response),
    );


    let btn = document.getElementById("registerButton");
    if(btn!=null)
      btn.addEventListener("click", (e:Event) => this.getUser());

    sessionStorage.setItem('token', '');
  }

handleSuccessfulResponse(response:User)
{
    this.result=response;
    console.log("OK");
}

getUser(): void {
      //this.httpClientService.getUser().subscribe(
     //response =>this.handleSuccessfulResponse(response),
    //);
        let url = 'http://localhost:8070/login';
        let response = this.httpClientService.httpClient.post<Observable<boolean>>(url, {
            name: "Amoeba",
            password: "321"
        }).subscribe(isValid => {
            if (isValid) {
                sessionStorage.setItem(
                  'token',
                  btoa(this.result.login + ':' + this.result.password)
                );
                this.router.navigate(['/user-home']);
            } else {
                alert("Authentication failed.");
            }
        });

}

}
