import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


export class User{
  constructor(
    public id:string,
    public login:string,
    public password:string,
  ) {}
}

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {
  constructor(public httpClient:HttpClient) {}

  getUser()
  {
    return this.httpClient.get<User>('http://localhost:8070/login');
  }

  getToken()
  {
      return this.httpClient.get<User>('http://localhost:8070/user');
  }
}
