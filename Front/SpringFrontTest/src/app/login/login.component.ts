import { Component, OnInit } from '@angular/core';
import { ClientService } from '../client.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

authRequest:any = {
  userName:"",
  password:"",
}

loginField:any;
passwordField:any;
 response:any;
 errorMessage : string;

  constructor(private service:ClientService, private router:Router) { }


  ngOnInit(): void {
      sessionStorage.setItem('token', '');
      sessionStorage.setItem('currentUserLogin', '');
      sessionStorage.setItem('currentUserId', '');

      this.loginField = document.getElementById("loginField");
      this.passwordField = document.getElementById("passwordField");

      let btn = document.getElementById("loginButton");
      if(btn!=null) btn.addEventListener("click", (e:Event) => this.getAccessToken(this.authRequest));

      let btnRegister = document.getElementById("registerButton");
      if(btnRegister!=null) btnRegister.addEventListener("click", (e:Event) => this.router.navigate(['/registration']));
  }

    public getAccessToken(authRequest : string){
    this.authRequest.userName = this.loginField.value;
    this.authRequest.password = this.passwordField.value;

      let resp=this.service.generateToken(authRequest);
      resp.subscribe(data=>this.accessApi(data));
    }

    public accessApi(data : any){

    let response = JSON.parse(data);

    if(response.token!='')
    {
      let resp=this.service.welcome(response.token);
      resp.subscribe(data=>this.router.navigate(['/home']));

      sessionStorage.setItem('token', response.token);
      sessionStorage.setItem('currentUserLogin', this.authRequest.userName);
      sessionStorage.setItem('currentUserId', response.userId);
    }
    else{
      this.errorMessage = "Invalid login/password";
    }
  }
}
