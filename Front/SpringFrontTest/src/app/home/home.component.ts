import { Component, OnInit } from '@angular/core';
import { ClientService } from '../client.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  constructor(private service:ClientService, private router:Router) { }

  homeUserName : string;
  searchField : any;
  errorMessage : any;
  contactsList : any;
  messagesList : any;
  sendMessageField : any;
  userId : any;

  currentCompanionName : string;

  homeRequest:any = {
    userName:"",
    token:""
  }

  userSearchRequest : any = {
    firstUserId:"",
    secondUserId:""
  }

  sendMessageRequest : any = {
    author:"",
    content:"",
    conversationId:""
  }

  ngOnInit(): void {
      this.loadHome();
      this.searchField = document.getElementById("searchInput");
      this.contactsList = document.getElementById("contactsList");
      this.messagesList = document.getElementById("messagesList");
      this.sendMessageField = document.getElementById("messageField");

      let btnSearch = document.getElementById("searchButton");
      if(btnSearch!=null) btnSearch.addEventListener("click", (e:Event) => this.searchForUser());

      let btnSend = document.getElementById("sendMessageButton");
      if(btnSend!=null) btnSend.addEventListener("click", (e:Event) => this.sendMessage());
  }

  public loadHome(){
      this.homeRequest.userName = sessionStorage.getItem('currentUserLogin');
      this.homeRequest.token = sessionStorage.getItem('token');
      this.userId = sessionStorage.getItem('currentUserId');
      let resp=this.service.loadHome(this.homeRequest.token, this.homeRequest);
      resp.subscribe(data=>this.setHomeData(data), error=>this.handleError(error));
  }

  private setHomeData(response : any){
      this.homeUserName = response;
      this.loadDialogs();
      this.addMessageToList("<MESSAGE>","<USER_NAME>");
  }

  private handleError(response : any)
  {
    this.router.navigate(['/login']);
  }

  private searchForUser()
  {
      this.userSearchRequest.firstUserId = sessionStorage.getItem('currentUserId');
      this.userSearchRequest.secondUserId = this.searchField.value;
      let resp=this.service.searchForUser(sessionStorage.getItem('token'), this.userSearchRequest);
      resp.subscribe(data=>this.addUser(data));
  }

  private addUser(data:any)
  {
      this.loadDialogs();
  }

  private loadDialogs(){
      this.contactsList.innerHTML = "";
      let resp=this.service.getUserConversations(sessionStorage.getItem('token'), sessionStorage.getItem('currentUserId'));
      resp.subscribe(data=>this.setDialogs(data));
  }

  private setDialogs(data : any){
      let response = JSON.parse(data);
      let conversations = response.conversations;

      for (var i=0; i<response.conversations.length; i++){
        let userIndex = 0;

        if(sessionStorage.getItem('currentUserId') == response.conversations[i].users[0]) userIndex = 1;
        else userIndex = 0;

        let userId = response.conversations[i].users[userIndex];
        let conversationId = response.conversations[i].id;
        let resp=this.service.getUserById(sessionStorage.getItem('token'), userId);
        resp.subscribe(data=>this.addDialogToList(data, conversationId));
      }
  }

  private addDialogToList(data : any, conversationId : any){
      let response = JSON.parse(data);
      var node = document.createElement("Button");//style="display:block"
      node.style.cssText = 'display:block';
      node.id = conversationId;
      node.addEventListener("click", (e:Event) =>
        {
          this.loadMessages(conversationId);
          sessionStorage.setItem('conversationId', conversationId);
          this.currentCompanionName = response.userName;
        });
      var textnode = document.createTextNode(response.userName);
      node.appendChild(textnode)
      this.contactsList.appendChild(node);
  }

  private loadMessages(conversationId : any){
      let resp=this.service.getMessages(sessionStorage.getItem('token'), conversationId);
      resp.subscribe(data=>this.setMessages(data));
  }

  private setMessages(data : any){
      let response = JSON.parse(data);
      let messages = response.messages;
      this.messagesList.innerHTML = "";

      for (var i=0; i<response.messages.length; i++){
        let authorName;
        if(response.messages[i].author == sessionStorage.getItem('currentUserId')) authorName = this.homeUserName;
        else authorName = this.currentCompanionName;
        this.addMessageToList(response.messages[i].content, authorName)
      }
  }

  private addMessageToList(content : any, author : any){
      var node = document.createElement("LI");
      var textnode = document.createTextNode(author + ": " + content);
      node.style.cssText = 'display:block';
      node.appendChild(textnode)
      this.messagesList.appendChild(node);
  }

  private sendMessage(){
      this.sendMessageRequest.author = sessionStorage.getItem('currentUserId');
      this.sendMessageRequest.content = this.sendMessageField.value;
      this.sendMessageRequest.conversationId = sessionStorage.getItem('conversationId');
      console.log(this.sendMessageRequest);

      let resp=this.service.sendMessage(sessionStorage.getItem('token'), this.sendMessageRequest);
      resp.subscribe(data=> {
        this.addMessageToList(this.sendMessageField.value, this.homeUserName);
        this.sendMessageField.value = '';
      });
  }
}
