import { Component, OnInit } from '@angular/core';
import { ClientService } from '../client.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  registerRequest:any = {
    userName:"",
    password:"",
    name:""
  }

  errorMessage : string;
  loginField:any;
  passwordField:any;
  nameField:any

  constructor(private service:ClientService, private router:Router) { }

  ngOnInit(): void {
      this.loginField = document.getElementById("loginField");
      this.passwordField = document.getElementById("passwordField");
      this.nameField = document.getElementById("nameField");

      let btnLogin = document.getElementById("loginButton");
      if(btnLogin!=null) btnLogin.addEventListener("click", (e:Event) => this.router.navigate(['/login']));

      let btnRegister = document.getElementById("registerButton");
      if(btnRegister!=null) btnRegister.addEventListener("click", (e:Event) => this.register());
  }

  private register(){
    this.registerRequest.userName = this.loginField.value;
    this.registerRequest.password = this.passwordField.value;
    this.registerRequest.name = this.nameField.value;

    let resp=this.service.register(this.registerRequest);
    resp.subscribe(data=>this.accessApi(data));
  }

  public accessApi(data : any){

    let response = JSON.parse(data);

    if(response.token!='')
    {
      console.log("REG");
      let resp=this.service.welcome(response.token);
      resp.subscribe(data=>this.router.navigate(['/home']));

      sessionStorage.setItem('token', response.token);
      sessionStorage.setItem('currentUserLogin', this.registerRequest.userName);
      sessionStorage.setItem('currentUserId', response.userId);
    }
    else{
      this.errorMessage = "Register error";
    }
  }
}
