import { Injectable } from '@angular/core';
import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private router: Router, private http: HttpClient) { }

  public generateToken(request : any){
    return this.http.post("http://localhost:9192/authenticate", request, {responseType: 'text' as 'json'})
  }

  public register(request : any){
    return this.http.post("http://localhost:9192/register", request, {responseType: 'text' as 'json'})
  }

  public welcome(token : any){
    let tokenStr = 'Bearer ' + token;
    const headers = new HttpHeaders().set('Authorization', tokenStr)
    return this.http.get("http://localhost:9192/", {headers, responseType: 'text' as 'json'});
  }

  public loadHome(token : any, request : any){
    let tokenStr = 'Bearer ' + token;
    const headers = new HttpHeaders().set('Authorization', tokenStr)
    return this.http.post("http://localhost:9192/home", request, {headers, responseType: 'text' as 'json'});
  }

  public searchForUser(token : any, request : any){
    let tokenStr = 'Bearer ' + token;
    const headers = new HttpHeaders().set('Authorization', tokenStr)
    return this.http.post("http://localhost:9192/addDialog", request, {headers, responseType: 'text' as 'json'});
  }

  public getUserConversations(token : any, userId : any){
    let tokenStr = 'Bearer ' + token;
    const headers = new HttpHeaders().set('Authorization', tokenStr)
    return this.http.get("http://localhost:9192/getDialogs?id=" + userId, {headers, responseType: 'text' as 'json'});
  }

    public getUserById(token : any, userId : any){
    let tokenStr = 'Bearer ' + token;
    const headers = new HttpHeaders().set('Authorization', tokenStr)
    return this.http.get("http://localhost:9192/getUserById?id=" + userId, {headers, responseType: 'text' as 'json'});
  }

  public getMessages(token : any, conversationID : any){
    let tokenStr = 'Bearer ' + token;
    const headers = new HttpHeaders().set('Authorization', tokenStr)
    return this.http.get("http://localhost:9192/getConversationMessages?id=" + conversationID, {headers, responseType: 'text' as 'json'});
  }

  public sendMessage(token : any, message : any){
    let tokenStr = 'Bearer ' + token;
    const headers = new HttpHeaders().set('Authorization', tokenStr)
    return this.http.post("http://localhost:9192/addMessage", message, {headers, responseType: 'text' as 'json'});
  }
}
