package com.example.demo.DTOs;

import com.example.demo.Conversation;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class FindConversationsResponseDTO {
    public List<Conversation> conversations;
}
