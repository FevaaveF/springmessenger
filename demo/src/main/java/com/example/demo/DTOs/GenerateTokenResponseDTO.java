package com.example.demo.DTOs;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class GenerateTokenResponseDTO {
    public String token;
    public String userId;
}
