package com.example.demo.DTOs;

import com.example.demo.Message;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@AllArgsConstructor
public class GetConversationMessagesResponseDTO {
    public List<Message> messages;
}
