package com.example.demo.DTOs;

public class SendMessageDTO {
    public Long author;
    public String content;
    public Long conversationId;
}
