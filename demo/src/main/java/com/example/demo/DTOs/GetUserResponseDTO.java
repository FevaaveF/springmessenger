package com.example.demo.DTOs;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class GetUserResponseDTO {
    public Long id;
    public String userName;
}
