package com.example.demo;

import com.example.demo.DTOs.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RestController
public class APIController {

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserRepository usersRepository;

    @Autowired
    private ConversationRepository conversationsRepository;

    @Autowired
    private MessageRepository messagesRepository;

    @GetMapping("/")
    public String welcome() {
        return "SUCCESS";
    }

    @PostMapping("/authenticate")
    public GenerateTokenResponseDTO generateToken(@RequestBody AuthRequestDTO authRequest) throws Exception {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authRequest.getUserName(), authRequest.getPassword())
            );
        } catch (Exception ex) {
            return new GenerateTokenResponseDTO("","");
        }

        String token = jwtUtil.generateToken(authRequest.getUserName());
        return new GenerateTokenResponseDTO(token,
                String.valueOf(usersRepository.findUserByLogin(authRequest.getUserName()).get().getId()));
    }

    @PostMapping("/register")
    public GenerateTokenResponseDTO register(@RequestBody RegisterRequestDTO registerRequest) throws Exception{
        Optional<User> checkUser
                = usersRepository.findUserByLoginAndPassword(registerRequest.userName, registerRequest.password);
        if(checkUser.isPresent()) return new GenerateTokenResponseDTO("","");

        User newUser = new User();
        newUser.setLogin(registerRequest.userName);
        newUser.setPassword(registerRequest.password);
        newUser.setName(registerRequest.name);
        usersRepository.save(newUser);

        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(newUser.getLogin(), newUser.getPassword())
            );
        } catch (Exception ex) {
            return new GenerateTokenResponseDTO("","");
        }

        String token = jwtUtil.generateToken(newUser.getLogin());
        return new GenerateTokenResponseDTO(token,
                String.valueOf(usersRepository.findUserByLogin(newUser.getLogin()).get().getId()));
    }

    @PostMapping("/home")
    public String loadHome(@RequestBody TokenCheckDTO tokenData){
        return  jwtUtil.validateToken(tokenData.token, tokenData.userName) ?
                usersRepository.findUserByLogin(tokenData.userName).get().getName() :
                "";
    }

    @PostMapping("/addDialog")
    public String addDialog(@RequestBody SearchUserDTO userData){
        Optional<Conversation> conversation = conversationsRepository.findConversationByUsers(
                Long.parseLong(userData.firstUserId),
                Long.parseLong(userData.secondUserId)
        );

        if(conversation.isPresent()){
            return "";
        }
        else {
            Conversation newConversation = new Conversation();
            newConversation.setUsers(
                    Arrays.asList(Long.parseLong(userData.firstUserId), Long.parseLong(userData.secondUserId)));
            newConversation.setMessages(Arrays.asList());

            conversationsRepository.save(newConversation);
            return usersRepository.findUserById(Long.parseLong(userData.secondUserId)).get().getName();
        }
    }

    @GetMapping("/getDialogs")
    public FindConversationsResponseDTO getDialogsForUser(@RequestParam(name = "id") String id){
        Optional<List<Conversation>> conversations = conversationsRepository.findAllConversationsForUser(
                Long.parseLong(id));
        return new FindConversationsResponseDTO(conversations.get());
    }

    @GetMapping("/getUserById")
    public GetUserResponseDTO getUserById(@RequestParam(name = "id") String id){
        Optional<User> user = usersRepository.findUserById(Long.parseLong(id));
        System.out.println(user.isPresent());
        return new GetUserResponseDTO(user.get().getId(), user.get().getName());
    }

    @GetMapping("/getConversationMessages")
    public GetConversationMessagesResponseDTO GetConversationMessages(@RequestParam(name = "id") String id){
        Optional<Conversation> conversation = conversationsRepository.findConversationById(Long.parseLong(id));

        GetConversationMessagesResponseDTO messages =
                new GetConversationMessagesResponseDTO(new ArrayList<Message>());

        for(int i=0; i<conversation.get().getMessages().size(); i++){
            messages.messages.add(messagesRepository.findMessageById(conversation.get().getMessages().get(i)).get());
            System.out.println(messages.messages.get(messages.messages.size()-1).getContent());
        }

        return messages;
    }

    @PostMapping("/addMessage")
    public void sendMessage(@RequestBody SendMessageDTO messageData){
        Message msg = new Message();
        msg.setAuthor(messageData.author);
        msg.setContent(messageData.content);
        Message result = messagesRepository.save(msg);
        conversationsRepository.addMessage(messageData.conversationId, result.getId());
    }
}
