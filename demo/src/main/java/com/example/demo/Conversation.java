package com.example.demo;

import com.vladmihalcea.hibernate.type.array.ListArrayType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@TypeDef(
        name = "list-array",
        typeClass = ListArrayType.class
)
public class Conversation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Type(type = "list-array")
    @Column(
            name = "users",
            columnDefinition = "bigint[]"
    )
    private List<Long> users;

    @Type(type = "list-array")
    @Column(
            name = "messages",
            columnDefinition = "bigint[]"
    )
    private List<Long> messages;
}