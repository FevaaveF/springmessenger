package com.example.demo;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface ConversationRepository extends CrudRepository<Conversation, Long> {

    //WON'T WORK FOR SELF DIALOG
    @Query(value = "select * from conversation where :a = ANY(users) AND :b = ANY(users)", nativeQuery = true)
    Optional<Conversation> findConversationByUsers(@Param("a") Long a, @Param("b") Long b);

    @Query(value = "select * from conversation where :userId = ANY(users)", nativeQuery = true)
    Optional<List<Conversation>> findAllConversationsForUser(@Param("userId") Long userId);

    @Transactional
    @Modifying
    @Query(value = "UPDATE conversation SET messages = array_append(messages, :msgId) WHERE id = :convId",
            nativeQuery = true)
    void addMessage(@Param("convId") Long convId, @Param("msgId") Long msgId);

    Optional<Conversation> findConversationById(Long id);
}