package com.example.demo;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    //User findByUserName(String username);
    Optional<User> findUserByLogin(String login);
    Optional<User> findUserById(Long id);
    Optional<User> findUserByLoginAndPassword(String login, String password);
}
